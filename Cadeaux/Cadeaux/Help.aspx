<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Help.aspx.cs" Inherits="CI_Metrics.About" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <br />

<html>

<head>
<meta http-equiv=Content-Type>
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 15">
<meta name=Originator content="Microsoft Word 15">
<link rel=File-List href="Help%20Tab_files/filelist.xml">
<link rel=Edit-Time-Data href="Help%20Tab_files/editdata.mso">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;
	mso-font-charset:1;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:0 0 0 0 0 0;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-536859905 -1073732485 9 0 511 0;}
@font-face
	{font-family:"Segoe UI";
	panose-1:2 11 5 2 4 2 4 2 2 3;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-520084737 -1073683329 41 0 479 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:0in;
	line-height:107%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;}
a:link, span.MsoHyperlink
	{mso-style-priority:99;
	color:#0563C1;
	mso-themecolor:hyperlink;
	text-decoration:underline;
	text-underline:single;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-noshow:yes;
	mso-style-priority:99;
	color:#954F72;
	mso-themecolor:followedhyperlink;
	text-decoration:underline;
	text-underline:single;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-link:"Balloon Text Char";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:9.0pt;
	font-family:"Segoe UI",sans-serif;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{mso-style-priority:34;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:.5in;
	mso-add-space:auto;
	line-height:107%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;}
p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
	{mso-style-priority:34;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-type:export-only;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	line-height:107%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;}
p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
	{mso-style-priority:34;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-type:export-only;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	line-height:107%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;}
p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
	{mso-style-priority:34;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-type:export-only;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:.5in;
	mso-add-space:auto;
	line-height:107%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;}
span.BalloonTextChar
	{mso-style-name:"Balloon Text Char";
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Balloon Text";
	mso-ansi-font-size:9.0pt;
	mso-bidi-font-size:9.0pt;
	font-family:"Segoe UI",sans-serif;
	mso-ascii-font-family:"Segoe UI";
	mso-hansi-font-family:"Segoe UI";
	mso-bidi-font-family:"Segoe UI";}
span.apple-converted-space
	{mso-style-name:apple-converted-space;
	mso-style-unhide:no;}
span.SpellE
	{mso-style-name:"";
	mso-spl-e:yes;}
span.GramE
	{mso-style-name:"";
	mso-gram-e:yes;}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	font-family:"Calibri",sans-serif;
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Calibri;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;}
.MsoPapDefault
	{mso-style-type:export-only;
	margin-bottom:8.0pt;
	line-height:107%;}
@page WordSection1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;
	mso-header-margin:.5in;
	mso-footer-margin:.5in;
	mso-paper-source:0;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 @list l0
	{mso-list-id:683089963;
	mso-list-type:hybrid;
	mso-list-template-ids:891467254 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l0:level1
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l0:level2
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l0:level3
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l0:level4
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l0:level5
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l0:level6
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l0:level7
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l0:level8
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l0:level9
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}
-->
</style>
</head>

<body>

<div class=WordSection1>

<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
style='font-size:14.0pt;mso-bidi-font-size:11.0pt;line-height:107%'>General
Data Entry Tips<o:p></o:p></span></b></p>

<p class=MsoNormal>Cells shaded BLUE signify ABILITY TO EDIT</p>

<p class=MsoNormal id="ContactUS">You can continue to edit blue cells, including cells you
have already edited, until you hit the submit button at the bottom of the form.
Once submitted, the information becomes static and you will not be able to
change it. If you need to correct any information following submission, you
will need to contact <asp:Label runat="server" ID="lblMailLink" Text="<a href=mailto:test@example.com?subject=Testing out mailto!>First Example</a>"></asp:Label>
.</p>

<p class=MsoNormal>At time of goal-setting, any metric not filled out will be
excluded in follow-up reporting. <i style='mso-bidi-font-style:normal'>Do not
fill out a goal for a metric that is not pertinent to your program.<o:p></o:p></i></p>

<p class=MsoNormal><b style='mso-bidi-font-weight:normal'>IMPORTANT: </b>Year-end
actuals (e.g. <b style='mso-bidi-font-weight:normal'>Y1-12 mo.</b>) should represent
the grand total for the entire twelve months, rather than just the term since
the 6-month report. In other words, the year-end numbers overwrite and replace
the mid-year numbers instead of being added to them.</p>

<p class=MsoNormal>Outcomes: At time of goal-setting, only the rate (e.g. 80%)
needs to be entered. At time of reporting, the details behind that rate
(numerator and denominator) are requested and <b style='mso-bidi-font-weight:
normal'>the rate is calculated for you</b>. Requesting the details allows us to
gauge how representative that outcome is of the population served. See the
section concerning <b style='mso-bidi-font-weight:normal'>Developing an Outcome
</b>to learn more.</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
style='font-size:14.0pt;mso-bidi-font-size:11.0pt;line-height:107%'>Common Metrics
(Outputs, Outcomes, and Narrative)<o:p></o:p></span></b></p>

<p class=MsoNormal><span style='color:black;background:white'>The Foundation
has developed a list of<span class=apple-converted-space>&nbsp;</span><strong><span
style='font-family:"Calibri",sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:"Times New Roman";
mso-bidi-theme-font:minor-bidi'>Common Metrics </span></strong><strong><span
style='font-family:"Calibri",sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:"Times New Roman";
mso-bidi-theme-font:minor-bidi;font-weight:normal;mso-bidi-font-weight:bold'>specific
to each focus area</span></strong>. We ask that you report on <span
class=GramE>any and all</span> common metrics<span class=apple-converted-space>&nbsp;</span><i>if</i><span
class=apple-converted-space>&nbsp;</span>you collect data concerning those
measures. It is not required for applicants to report on a common metric if
your organization does not collect data concerning that measure or if it is not
relevant to the request.</span></p>

<p class=MsoNormal><i style='mso-bidi-font-style:normal'>The programs and
initiatives within each focus area vary far more than what is captured by the
few common metrics shown here. Therefore, it should NOT be perceived that a
lack of common metrics selected is an indication of lack of importance or
strategic fit. We rely on individual metrics to describe the impact of
individual programs.<o:p></o:p></i></p>

<p class=MsoNormal>All programs should fill out <b style='mso-bidi-font-weight:
normal'>Unduplicated Individuals</b>, <b style='mso-bidi-font-weight:normal'>Encounters</b>,
and <b style='mso-bidi-font-weight:normal'>Inclusion Criteria</b> (found under
the Common Metrics - Narrative section).</p>

<p class=MsoNormal>-<b style='mso-bidi-font-weight:normal'>Unduplicated
Individuals</b> represent unique people served (excluding repeat visits or
points of contact).</p>

<p class=MsoNormal>-<b style='mso-bidi-font-weight:normal'>Encounters</b>
represent all visits or �points of contact� as defined by your organization</p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><u>Filling out �Inclusion Criteria for Unduplicated Individuals and
Encounters�<o:p></o:p></u></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span style='mso-ascii-font-family:Calibri;mso-fareast-font-family:
Calibri;mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri'>While it
may seem straight-forward within your organization, different programs think
about �points of contact� differently. In this section, we are asking you to
describe �what counts� as an individual served and as an encounter. Does it
strictly include individuals who received treatment or does it also include
outreach events like health fairs? <i style='mso-bidi-font-style:normal'>- our
personal preference is for the former in this example.</i><o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span style='mso-ascii-font-family:Calibri;mso-fareast-font-family:
Calibri;mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri'>This
section is also the opportunity to describe any challenges or estimates used when
calculating individuals and encounters. For example, unknown overlap between
two separate programs within the organization due to a lack of identifying
information collected. See below for example responses:<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span style='mso-ascii-font-family:Calibri;mso-fareast-font-family:
Calibri;mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><b style='mso-bidi-font-weight:normal'><span style='mso-ascii-font-family:
Calibri;mso-fareast-font-family:Calibri;mso-hansi-font-family:Calibri;
mso-bidi-font-family:Calibri'>Example of a helpful response:<o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span style='mso-ascii-font-family:Calibri;mso-fareast-font-family:
Calibri;mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri'>Unduplicated
Individuals and Encounters represent anyone who received treatment from
clinicians as well as counseling from social workers. It does not include
individuals screened for eligibility but who ultimately did not receive
services. Because counseling is confidential and a unique id is not assigned,
we do not know how much overlap exists between programs. Therefore, encounters
entered is exact but unduplicated individuals is an estimation.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span style='mso-ascii-font-family:Calibri;mso-fareast-font-family:
Calibri;mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><b style='mso-bidi-font-weight:normal'><span style='mso-ascii-font-family:
Calibri;mso-fareast-font-family:Calibri;mso-hansi-font-family:Calibri;
mso-bidi-font-family:Calibri'>Example of a less-helpful response:<o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span style='mso-ascii-font-family:Calibri;mso-fareast-font-family:
Calibri;mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri'>Unduplicated
Individuals and Encounters are pulled from our database, called <span
class=SpellE>OmegaSystem</span>, by our Senior Database Specialist.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span style='mso-ascii-font-family:Calibri;mso-fareast-font-family:
Calibri;mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
style='font-size:14.0pt;mso-bidi-font-size:11.0pt;line-height:107%'>Individual Metrics<o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span style='color:black;background:white'>Each applicant can submit up
to three outputs and up to three outcomes, developed by your organization, that
best describes the intended impact of the project<i style='mso-bidi-font-style:
normal'>.</i></span><i style='mso-bidi-font-style:normal'> Do not duplicate any
metrics already reported in the Common Metrics section.<o:p></o:p></i></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span style='color:black;background:white'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span style='color:black;background:white'>Outputs describe what the
program does and the different activities provided by staff.<br>
<br>
Outcomes describe the impact (knowledge gained, behavior/attitudes changed, or
health status improved) of those activities or of the program. Goals for
outcomes should always be represented as a percentage.</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><b style='mso-bidi-font-weight:normal'>Goal Setting<o:p></o:p></b></p>

<p class=MsoNormal>Define each metric under the description column, including defining
the numerator and denominator for outcomes. Set a goal for each metric. For
outcomes, the only number needed for goals is the percentage or rate.
Numerators and denominators do not need a number defined at goal-setting. If
you are submitting a multi-year grant request, set goals for both Year 1 and
Year 2.</p>

<p class=MsoNormal><b style='mso-bidi-font-weight:normal'>Reporting<o:p></o:p></b></p>

<p class=MsoNormal>If you are returning to this Metrics Web Tool to report, you
will find that all goals and individualized metrics have been saved within the
tables. If the tables do not match information submitted at the time of
goal-setting please contact <asp:Label runat="server" ID="lblMailLink2" Text="<a href=mailto:test@example.com?subject=Testing out mailto!>First Example</a>"></asp:Label>
.</p>

<p class=MsoNormal><b>6-month report</b> metrics should describe the first six
months of the grant term (Jan 1 - Jun 30 or Jul 1 - Dec 31).</p>

<p class=MsoNormal><b>12-month report</b> metrics should describe the <b
style='mso-bidi-font-weight:normal'>entire year</b> of the grant term (Jan 1 -
Dec 31 or Jul 1 - Jun 30), <b style='mso-bidi-font-weight:normal'>NOT</b> just
the term following the six-month report. In other words, the year-end numbers
overwrite and replace the mid-year numbers instead of being added to them.</p>

<p class=MsoNormal>Enter the numerator and denominator for outcomes and <b
style='mso-bidi-font-weight:normal'>the rate will be calculated for you</b>.</p>

<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
style='font-size:14.0pt;mso-bidi-font-size:11.0pt;line-height:107%'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
style='font-size:14.0pt;mso-bidi-font-size:11.0pt;line-height:107%'>Developing
an Outcome<o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'>Outcomes describe the result of your project in the form of a percentage,
which usually represents the proportion of participants who reach a specific
milestone within the program. All percentages are calculated from a fraction
which include a numerator and denominator. For example, 1 out of 4 = 25%, where
1 is the numerator and 4 is the denominator.</p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'>For outcomes, the percentage represents the proportion of clients who
reach �success� as defined by your program. The numerator represents the number
of participants who were successful during the report timeframe. The
denominator represents all individuals who were eligible for success or had the
opportunity to reach success during the report timeframe (and who filled out
the measurement tool). A simple example below:</p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span style='mso-no-proof:yes'><v:shapetype id="_x0000_t75"
 coordsize="21600,21600" o:spt="75" o:preferrelative="t" path="m@4@5l@4@11@9@11@9@5xe"
 filled="f" stroked="f">
 <v:stroke joinstyle="miter" xmlns:v="urn:schemas-microsoft-com:vml"/>
 <v:formulas>
  <v:f eqn="if lineDrawn pixelLineWidth 0" xmlns:v="urn:schemas-microsoft-com:vml"/>
  <v:f eqn="sum @0 1 0" xmlns:v="urn:schemas-microsoft-com:vml"/>
  <v:f eqn="sum 0 0 @1" xmlns:v="urn:schemas-microsoft-com:vml"/>
  <v:f eqn="prod @2 1 2" xmlns:v="urn:schemas-microsoft-com:vml"/>
  <v:f eqn="prod @3 21600 pixelWidth" xmlns:v="urn:schemas-microsoft-com:vml"/>
  <v:f eqn="prod @3 21600 pixelHeight" xmlns:v="urn:schemas-microsoft-com:vml"/>
  <v:f eqn="sum @0 0 1" xmlns:v="urn:schemas-microsoft-com:vml"/>
  <v:f eqn="prod @6 1 2" xmlns:v="urn:schemas-microsoft-com:vml"/>
  <v:f eqn="prod @7 21600 pixelWidth" xmlns:v="urn:schemas-microsoft-com:vml"/>
  <v:f eqn="sum @8 21600 0" xmlns:v="urn:schemas-microsoft-com:vml"/>
  <v:f eqn="prod @7 21600 pixelHeight" xmlns:v="urn:schemas-microsoft-com:vml"/>
  <v:f eqn="sum @10 21600 0" xmlns:v="urn:schemas-microsoft-com:vml"/>
 </v:formulas>
 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect" xmlns:v="urn:schemas-microsoft-com:vml"/>
 <o:lock v:ext="edit" aspectratio="t" xmlns:o="urn:schemas-microsoft-com:office:office"/>
</v:shapetype><v:shape id="Picture_x0020_1" o:spid="_x0000_i1025" type="#_x0000_t75"
 style='width:474.75pt;height:76.5pt;visibility:visible;mso-wrap-style:square'>
 <v:imagedata src="Help%20Tab_files/image001.png" o:title="Help Table" xmlns:v="urn:schemas-microsoft-com:vml"/>
</v:shape></span>
    <img alt="" class="auto-style1" src="Content/Help%20Table.png" /></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><b style='mso-bidi-font-weight:normal'>Common Mistakes: </b>Some programs
tend to put everyone served in the denominator. While this is appropriate in
some cases, many times this underestimates the impact of the program. A good
rule of thumb would be to exclude anyone where success status is unknown from
both the numerator and denominator. Improving survey return rates should be an
important goal of the organization, but remain separate from measures of
impact. Likewise, if it is common knowledge that a certain timeframe is
required to achieve success, all participants who haven�t been in the program
long enough to achieve success should be excluded.</p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'>Generally, an outcome that comes out to or near 100% for the population
served is not one that is very meaningful. Most programs are tackling
complicated and entrenched social issues. It is generally understood that for
genuine and long-lasting change to occur, more people will need to be reached
and a subset of that population will attain the change you are pursuing.</p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
style='font-size:14.0pt;mso-bidi-font-size:11.0pt;line-height:107%'>Example
Outcome Formats<o:p></o:p></span></b></p>

<p class=MsoNormal>We have described the outcome format that is used <b
style='mso-bidi-font-weight:normal'>most often</b>:</p>

<p class=MsoNormal><u>Proportion of Clients Successful<o:p></o:p></u></p>

<p class=MsoNormal>Example: Proportion of clients who report an improved sense
of self-worth.<br>
Numerator: Clients who achieve success (improved knowledge, attitude, or
behavior).<br>
Denominator: Eligible clients who were measured (attended minimum sessions and
returned a survey). </p>

<p class=MsoNormal>There are two, <b style='mso-bidi-font-weight:normal'>less
common</b>, outcome formats that are slightly more sophisticated:</p>

<p class=MsoNormal><u>Average Percent Increase of Clients (Pre- and Post-test
needed)<o:p></o:p></u></p>

<p class=MsoNormal>Example: On average, our participants will experience a 10%
increase in school attendance rates.<br>
Numerator: Average New Rate minus Average Old Rate (88 � 80 = 8).<br>
Denominator: Average Old Rate (80).<br>
Percent Increase: 10%</p>

<p class=MsoNormal><u>Average Score of All Participants (no Pre-test available)<br>
</u>Example: The average GPA of all students in program will be 3.0 or higher at
the end of the program.<br>
Numerator: Sum of all scores (3,000).<br>
Denominator: Total participants (1,000).<br>
Average Score: 3.0</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>If you need assistance developing metrics or using this
tool, please reach out to your program officer or the <a
href="mailto:jsimmons@stdavidsfoundation.org">evaluation staff</a> of St.
David�s Foundation.</p>

</div>

</body>

</html>

</asp:Content>