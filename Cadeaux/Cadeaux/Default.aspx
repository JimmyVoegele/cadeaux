﻿<%@ Page Title="Cadeaux -  GIFTS Online Web Application" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Cadeaux._Default" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">

    </telerik:RadAjaxManager>
    <br />
    <h2 style="color:#0091ba">Cadeaux - A GIFTS Online Web Application</h2>
    <asp:Label ID="lblGrantSubmitted" runat="server" Text="Your Metrics have been submitted successfully." Visible="True" ForeColor="Red" Font-Size="Large"></asp:Label>

</asp:Content>
