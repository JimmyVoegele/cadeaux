﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cadeaux
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string str = GetLoggedInUser();
        }

        private string GetLoggedInUser()
        {
            string strLoggedInUserID = HttpContext.Current.User.Identity.Name;

            //Debug Only
            //strLoggedInUserID = "jimmy.voegele@gmail.com";

            if (string.IsNullOrEmpty(strLoggedInUserID) || strLoggedInUserID.ToUpper().Contains("SDFBUILD"))
            {
                strLoggedInUserID = "jimmy.voegele1@gmail.com";
            }

            string myConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            VerifyRequestID(myConnectionString);

            lblGrantSubmitted.Text = "Logged in user:" + strLoggedInUserID;

            return strLoggedInUserID;

        }

        private string VerifyRequestID(string connString)
        {
            string strNewRequestID = string.Empty;

            string selectSQL = "Select * from [dbo].[vwGrantRequests]";

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strNewRequestID = reader["GReqID"].ToString();
                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }



            return strNewRequestID;
        }
    }
}