﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CI_Metrics
{
    public partial class Contact : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMailLink2.Text = "<a href = \"mailto:grantsinfo@stdavidsfoundation.org?subject=Metrics Application Question – Grant ID " + Session["REQUESTID"] + "\">grantsinfo@stdavidsfoundation.org</a>";
        }
    }
}