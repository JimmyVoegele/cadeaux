﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="CI_Metrics.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <h2 style="color:#0091ba"><%: Title %>.</h2>
    <br />
    <h3>St. David's Foundation Community Investments Team</h3>
    <address>
        1303 San Antonio Street Suite 500<br />
        Austin, TX 78701<br />
        <abbr title="Phone">P:</abbr>
        512.879.6600
    </address>

    <address>
        <asp:Label runat="server" ID="lblMailLink2" Text="<a href=mailto:test@example.com?subject=Testing out mailto!>First Example</a>"></asp:Label>
    </address>
</asp:Content>
